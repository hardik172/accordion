import React, { useState } from "react";
import Convert from "./Convert";
import Dropdown from "./Dropdown";
import Input from "./UIcomponents/Input";

const options = [
    {
        label: 'Gujarati',
        value: `gu`
    },
    {
        label: 'Hindi',
        value: 'hi'
    },
    {
        label: 'Panjabi',
        value: 'pa'
    },
    {
        label: 'Marathi',
        value: 'mr'
    },
    {
        label: 'Tamil',
        value: 'ta'
    },
    {
        label: 'Afrikaans',
        value: 'af'
    }
]

const Translate = () => {
    const [language, setLanguage] = useState(options[0]);
    const [text, setText] = useState('');
    return (
        <div>
            <Input label="Enter a text" value={text} onChange={(e) => setText(e.target.value)} />
            <Dropdown
                options={options}
                selected={language}
                onSelectedChange={setLanguage}
                label="Select a language"
            />
            <hr />
            <h3 className="ui header">Output</h3>
            <Convert text={text} language={language} />

        </div>
    );
};

export default Translate;