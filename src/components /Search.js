import { useState, useEffect } from "react";
import React from "react";
import axios from "axios";

const Search = () => {

    const [searchTerm, setSearchTerm] = useState('usa');

    const handleChange = (event) => {
        event.preventDefault();

        console.log(event.target.value);
        setSearchTerm(event.target.value);
    }

    useEffect(() => {
        const search = async () => {
            await axios.get('https://en.wikipedia.org/w/api.php', {
                params: {
                    actions: 'query',
                    list: 'search',
                    origin: '*',
                    format: 'json',
                    srsearch: searchTerm,
                },
            });
        };

        search();
    }, [searchTerm]);


    return (
        <div>
            <div className="ui form">
                <div className="field">
                    <label>Search</label>
                    <input
                        className="input"
                        onChange={handleChange}
                        value={searchTerm}
                    />
                </div>
            </div>
        </div>
    );
}
export default Search;