import React from "react";

const Input = ({ label, value, onChange }) => {
    return (
        <div className="ui form">
            <div className="field">
                <div className="label">{label}</div>
                <div> <input value={value} onChange={onChange} /> </div>
            </div>
        </div>
    );
};

export default Input;
