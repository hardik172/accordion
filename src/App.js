import React, { useState } from "react";
import Accordion from "./components /Accordion";
import Dropdown from "./components /Dropdown";
import Header from "./components /Header";
import Route from "./components /Route";
import Search from "./components /Search";
import Translate from "./components /Translate";

const items = [
    {
        title: "What is react?",
        content: "React is a Javascript Library"
    },
    {
        title: "Is is open source?",
        content: "Yes, it was created by Facebook in 2012."
    },
    {
        title: "Why use react?",
        content: "React is a favourite language amongst engineers."
    }
]

const options = [
    {
        label: "The Color Red",
        value: "red"
    },
    {
        label: "The color Green",
        value: "green"
    },
    {
        label: "Shaded of Blue",
        value: "blue"
    }
];

// old method for navigating app
// const showAccordion = () => {
//     if (window.location.pathname === '/') {
//         return <Accordion items={items} />
//     };
// };

// const showList = () => {
//     if (window.location.pathname === '/list') {
//         return <Search></Search>
//     };
// };

// const showDropdown = () => {
//     if (window.location.pathname === '/dropdown') {
//         return <Dropdown />
//     };
// };

// const showTranslate = () => {
//     if (window.location.pathname === '/translate') {
//         return <Translate />
//     }
// };
const App = () => {
    const [selected, setSelected] = useState(options[0]);
    return (
        <div className="ui justified container">
            <Header />
            <Route path='/'>
                <Accordion items={items} />
            </Route>
            <Route path="/list">
                <Search></Search>
            </Route>
            <Route path='/dropdown'>
                <Dropdown
                    label="Select a color"
                    options={options}
                    selected={selected}
                    onSelectedChange={setSelected}
                ></Dropdown>
            </Route>
            <Route path='/translate'>
                <Translate></Translate>
            </Route>


            {/* {showAccordion()}
            {showList()}
            {showDropdown()}
            {showTranslate()} */}

        </div>
    );
};

export default App;
